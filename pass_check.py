# Check ability of gpg to decrypt passwords converted from pwdata.
import os
import subprocess

DATA_DIR = '/home/jwbwater/.password-store'
PWDATA_FILE = '/home/jwbwater/.pwdata/pwdata.gpg'

# count passwords in pwdata
process = subprocess.run(
    f'gpg -d {PWDATA_FILE}',
    capture_output=True,
    shell=True,
)
entries = process.stdout.decode().split('\n')
# remove empty last entry
del(entries[-1])
# generate list of passwords
passwords = [entry.split(', ')[2] for entry in entries]
num_entries = len(entries)
print(f'Total number of passwords in pword: {num_entries}')


# walk directory tree
total_files = 0
errors = 0
for path, directories, files in os.walk(DATA_DIR):
    if '.git' not in path:
        # print(path)
        for filename in files:
            filepath = os.path.join(path, filename)
            for char in ["(", ")", "'"]:
                filepath = filepath.replace(char, '\\' + char)
            if '.gpg' in filename[-4:]:
                total_files += 1
                process = subprocess.run(
                    f'gpg -d {filepath}',
                    capture_output=True,
                    shell=True,
                )
                if 'fail' in process.stderr.decode().lower():
                    errors += 1
                    print(filepath, process.stderr.decode())
                password = process.stdout.decode()
                if password in passwords:
                    # remove from list of pword entries
                    p = passwords.index(password)
                    del(entries[p])
                    del(passwords[p])
                else:
                    print(f'Extra pass entry: {filepath}')
            else:
                print(f"Found unencrypted file {filepath}")
print(f'Total number of passwords in pass: {total_files}')
percent = 100 * errors / total_files
print(f'Percent of files that could not be decrypted: {percent:.3}%')

# check for left over pword entries
for entry in entries:
    print(entry)
