"""
Password management

This program uses the bitwarden CLI to retrieve, add, remove, and update login credentials.

Usage Model:

Password retrieval:

    Passwords are retrieved using "pword <pattern>". If the search matches only one entry, the
    password from this entry is copied to the clipboard. If it matches multiple entries, the names
    of the matching entries are listed so the user can try a new pattern that matches only a single
    entry.

Adding a new entry:

    Passwords are added by entering an identifying name (e.g. website name), and a username. The
    password is auto-generated.

Updating an entry:

    The entry name and username can be changed using "pword -e <pattern>"

"""


import click
import configparser
import getpass
import json
import os
import pyperclip
import random
import string
import subprocess
import sys
import time

from multiprocessing import Process


BW_SESSION_FILE = os.path.join(os.environ["HOME"], ".config", "pword", "bw_session")
CONFIG_FILE = os.path.join(os.environ["HOME"], ".config", "pword", "config")


# read ini file to get local and remote repo locations
config = configparser.ConfigParser()
config.read(CONFIG_FILE)
EMAIL = config["USER"]["EMAIL"]


def add_item(password_length, no_symbols, numbers_only):
    """Add a password entry."""
    item_name = input("Name of entry: ")
    username = input("User name: ")
    password = generate_password(
        password_length,
        no_symbols=no_symbols,
        numbers_only=numbers_only,
    )
    item = {
        "organizationId": None,
        "collectionIds": None,
        "folderId": None,
        "type": 1,
        "name": item_name,
        "notes": "",
        "favorite": False,
        "fields": [],
        "login": {
            "username": username,
            "password": password,
        },
        "secureNote": None,
        "card": None,
        "identity": None,
        "reprompt": 0,
    }
    process = subprocess.Popen(
        [bitwarden_path(), "encode"],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True,
    )
    stdout, stderr = process.communicate(input=json.dumps(item))
    encoded_json = stdout
    bitwarden("create", "item", encoded_json)
    get_entry(item_name)


def bitwarden(*args):
    """Call bitwarden CLI"""
    # make sure BW_SESSION_FILE exists
    subprocess.run(["touch", BW_SESSION_FILE])
    with open(BW_SESSION_FILE) as f:
        bw_session_id = f.read()
    try:
        process = subprocess.Popen(
            [bitwarden_path(), "--session", bw_session_id, *args],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        stdout, stderr = process.communicate(timeout=3)
    except subprocess.TimeoutExpired:
        process.terminate()
        stdout, stderr = process.communicate()
    if stderr:
        if stderr == "You are not logged in.":
            bitwarden_login()
            return bitwarden(*args)
        elif "Master password:" in stderr:
            bitwarden_unlock()
            return bitwarden(*args)
        else:
            print(stderr)
            sys.exit()
    return stdout


def bitwarden_lock(delay):
    """
    Lock bitwarden vault so master password is required after delay seconds.
    """
    if os.fork() != 0:
        # exit parent process
        return
    time.sleep(delay)
    bitwarden("lock")


def bitwarden_login():
    """Login to bitwarden and save bw_session environment variable"""
    bitwarden_save_bw_session([bitwarden_path(), "login", EMAIL])


def bitwarden_path():
    """Return the path to the bitwarden, bw executible"""
    process = subprocess.run(
        ["which", "bw"],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True,
    )
    if process.returncode != 0:
        print(process.stderr)
        sys.exit()
    # path_var = [var for var in process.stdout.split("\n") if var.split("=")[0] == "PATH"]
    return process.stdout.strip()


def bitwarden_save_bw_session(command):
    """Login or unlock bitwarden vault and save bw_session environment variable"""
    password = getpass.getpass("Master password: ")
    process = subprocess.run(
        command + [password],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True,
    )
    if process.returncode != 0:
        print(process.stderr)
        sys.exit()
    bw_session_id = [w for w in process.stdout.split("\n") if "env:BW_SESSION" in w][
        0
    ].split("=", maxsplit=1)[1]
    # write bitwarden session ID to file
    with open(BW_SESSION_FILE, "w") as f:
        f.write(bw_session_id)
    print("Bitwarden vault will lock in 10 minutes")
    Process(target=bitwarden_lock, args=(600,)).start()


def bitwarden_search(pattern):
    """Search bitwarden for item names matching pattern"""
    # bitwarden includes items if the username includes pattern, only include items where the
    # item name includes pattern
    bitwarden_matches = bitwarden("list", "items", "--search", pattern)
    if bitwarden_matches == "":
        print("Empty bitwarden response")
        sys.exit(0)
    matching_items = [
        item
        for item in json.loads(bitwarden_matches)
        if pattern.lower() in item["name"].lower()
    ]
    if not matching_items:
        print("No matching entry found")
        sys.exit(0)
    if len(matching_items) > 1:
        # There are multiple matching entries, print the entry name and username for each.
        for matching_item in matching_items:
            username = ""
            if matching_item.get("login"):
                username = matching_item.get("login").get("username")
            print(f"{matching_item['name']:<40} {username}")
        sys.exit(0)

    return matching_items[0]


def bitwarden_unlock():
    """Unlock bitwarden vault and save bw_session environment variable"""
    bitwarden_save_bw_session([bitwarden_path(), "unlock"])


def clear_clipboard(password, delay):
    """
    After delay seconds, if clipboard still contains password, clear it.
    """
    if os.fork() != 0:
        # exit parent process
        return
    time.sleep(delay)
    if paste() == password:
        copy("")


def copy(text):
    """Copy text to clipboard"""
    if os.environ["XDG_SESSION_TYPE"] == "wayland":
        subprocess.run(["wl-copy", text])
    else:
        pyperclip.copy(text)


def edit_entry(search_string):
    """Edit the name or username of a password entry."""
    # search bitwarden items for search_string
    matching_item = bitwarden_search(search_string)
    print(
        "Enter new entry name or press enter to keep",
        matching_item["name"],
        ": ",
        end="",
    )
    entry_name = input()
    matching_item["name"] = entry_name or matching_item["name"]
    print(
        "Enter new username or press enter to keep",
        matching_item["login"]["username"],
        ": ",
        end="",
    )
    username = input()
    matching_item["login"]["username"] = username or matching_item["login"]["username"]
    process = subprocess.Popen(
        [bitwarden_path(), "encode"],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True,
    )
    stdout, stderr = process.communicate(input=json.dumps(matching_item))
    encoded_json = stdout
    bitwarden("edit", "item", matching_item["id"], encoded_json)


def generate_password(length, no_symbols=False, numbers_only=False):
    """Generate a random password."""
    special_characters = string.punctuation
    if no_symbols is True:
        # use all special characters by default
        special_characters = ""
    # generate the random password and copy it to the clipboard
    if numbers_only is True:
        chars = string.digits
    else:
        chars = (
            string.ascii_lowercase
            + string.ascii_uppercase
            + string.digits
            + special_characters
        )
    pw = "".join(random.choice(chars) for x in range(length))
    return pw


def get_entry(search_string):
    """Get a password."""
    # search bitwarden items for search_string
    matching_item = bitwarden_search(search_string)
    # There is only one match so copy the password to the clipboard.
    password = matching_item["login"]["password"]
    copy(password)
    print(
        f"The password for {matching_item['name']}/{matching_item['login']['username']} "
        "was copied to the clipboard. It will be cleared in one minute."
    )
    # wait for a minute and clear the clipboard
    Process(target=clear_clipboard, args=(password, 60)).start()


def paste():
    """Get text from clipboard"""
    if os.environ["XDG_SESSION_TYPE"] == "wayland":
        process = subprocess.Popen(
            ["wl-paste"],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        stdout, stderr = process.communicate(timeout=3)
        return stdout.strip()
    else:
        return pyperclip.paste()


@click.command()
@click.pass_context
@click.argument("search_string", required=False)
@click.option("--add", type=int, help="add a password entry")
@click.option("--edit", help="edit password entry <search_string>")
@click.option("--no-symbols", is_flag=True, help="generate password without symbols")
@click.option("--numbers-only", is_flag=True, help="generate numeric password")
@click.option(
    "--generate", type=int, default=None, help="generate random password <length>"
)
@click.option("--keystrokes", is_flag=True, help="simulate typing password")
@click.option("--remove", help="remove password entry <search_string>")
@click.option("--sync", is_flag=True, help="sync vault with bitwarden")
def pword(
    ctx,
    search_string,
    add,
    edit,
    generate,
    keystrokes,
    no_symbols,
    numbers_only,
    remove,
    sync,
):
    """Password manager"""
    if add:
        add_item(password_length=add, no_symbols=no_symbols, numbers_only=numbers_only)
    elif edit:
        edit_entry(edit)
    elif generate:
        copy(
            generate_password(
                length=generate, no_symbols=no_symbols, numbers_only=numbers_only
            )
        )
        print("A randomly generated password was copied to the clipboard.")
    elif keystrokes:
        type_password()
    elif remove:
        remove_entry(remove)
    elif search_string:
        get_entry(search_string)
    elif sync:
        bitwarden("sync")
    else:
        click.echo(ctx.get_help())


def remove_entry(search_string):
    """Remove a password entry."""
    # search bitwarden items for search_string
    matching_item = bitwarden_search(search_string)
    print(
        "PERMANENTLY REMOVE the entry for "
        f"{matching_item['name']}/{matching_item['login']['username']} (y/N): ",
        end="",
    )
    answer = input()
    if answer.lower() == "y":
        # delete the entry
        bitwarden("delete", "item", matching_item["id"])
    else:
        print("No password entry was removed.")


def type_password():
    """
    Simulate typing password for websites that block pasting.
    I bind this to Ctrl-Alt-v in XFCE's keyboard settings.
    """
    password = paste()
    subprocess.run(["xdotool", "type", password])


if __name__ == "__main__":
    pword(sys.argv[1:])
