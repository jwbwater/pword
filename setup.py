from setuptools import setup

setup(
    name='pword',
    version='0.1.0',
    description='Password manager.',
    py_modules=['pword'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'click',
        'flake8',
        'ipdb',
        'pyperclip',
        'pytest',
        'pytest-cov',
        'python-dotenv',
    ],
    entry_points={
        'console_scripts': [
            'pword = pword:pword'
        ]
    },
)
