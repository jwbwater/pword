# pword

Command line password manager written in Python3. To provide access from multiple machines, the passwords are stored in a GPG encrypted file in a separate, private repo on GitLab. This also provides two layers of security since attackers would have to get access to the private GitLab repo (GitLab password or SSH key) and the GPG key and password.

## Installation
```
git clone git@gitlab.com:jwbwater/pword.git
sudo pip3 install .
```

## Dependencies

wl-clipboard : copy and paste to Wayland clipboard
xdotool : simulate keystrokes for websites that block pasting in password fields

Have yet to find an xdotool substitute for use with Wayland and Gnome.
wtype: does not work with gnome
ydotool: requires a daemon and extensive use of sudo
dotool: not audited by anyone

In Fedora:
```
sudo dnf install wl-clipboard
```

## Usage
To get the username and password for gmail.
```
pword gmail
```
