"""
Convert .pwdata to .password-store format.

This will enable me to take advantage of the password-store ecosystem, like
the iOS app.
"""
import subprocess
import os

DATA_DIR = '/home/jwbwater/.password-store'
PWDATA_FILE = '/home/jwbwater/.pwdata/pwdata.gpg'


ans = input(f"Type yes to OVER WRITE {DATA_DIR}? ")
if "yes" in ans.lower():
    # go through .pwdata line by line and parse into website name, username,
    # and password.
    process = subprocess.run(
        f'gpg -d {PWDATA_FILE}',
        capture_output=True,
        shell=True,
    )
    lines = process.stdout.decode().split('\n')
    for line in lines:
        words = line.split(', ')
        if len(words) == 3:
            entry, username, password = line.split(', ')
            entry = entry.strip()
            entry = entry.replace(' ', '-')
            directory = os.path.join(DATA_DIR, entry)
            username = username.replace(' ', '')
            filename = username
            password = password.strip()
            # if multiple usernames use the last as the filename
            # add previous usernames to directory path
            if '/' in username:
                usernames = username.split('/')
                filename = usernames[-1]
                directory = os.path.join(directory, *usernames[:-1])
            # if no username use entry as filename
            if username.lower() in ['', 'n/a']:
                directory = os.path.join(DATA_DIR)
                filename = entry
            # make a directory for each entry
            subprocess.run(["mkdir", "-p", directory])
            # write password to username file
            filepath = os.path.join(directory, filename)
            print(filepath)
            with open(filepath, 'w') as gpg:
                gpg.write(password)
            # encrypt username file
            subprocess.run(['gpg', '-r', 'jwbwater', '-e', filepath])
            # encrypt username file
            subprocess.run(['rm', filepath])
